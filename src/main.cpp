/**
******************************************************************************
* @author	  Arduino development
* @version  1.0
* @date		  18.10.2019
* @brief	  Stopwatch with 5 modes and double channel relay
*		
*/

#define SERIAL_DEBUG_OUT
#define SERIAL_BAUDRATE 9600

#define TIME1 900
#define TIME2 1200
#define TIME3 1500
#define TIME4 1800
#define TIME5 5400
#define TIME5_SEC 5
#define TIME_SPECIAL 1800

#define DEBOUNCE 10
#define CLICK_TICKS 12
#define PRESS_TICKS 3000

#define DISPLAY_CLK 12
#define DISPLAY_DATA 11

#include <Arduino.h>
#include <SimpleRelay.h>
#include <TM1637Display.h>
#include <ObjectButton.h>
#include <interfaces/IOnClickListener.h>
#include <interfaces/IOnPressListener.h>

typedef enum{
  MODE_1 = 1, //closed relay1 for 15min
  MODE_2, //closed relay1 for 20min
  MODE_3, //closed relay1 for 25min
  MODE_4, //closed relay1 for 30min
  MODE_5  //closed relay1 for 90min; every 30 min closed relay2 for 5 sec
}Mode;

typedef enum{
  SEC = 1,
  MIN_SEC
}DisplayMode;

constexpr static byte BUTTON1 = A1;
constexpr static byte BUTTON2 = A2;
constexpr static byte BUTTON3 = A3;
constexpr static byte BUTTON4 = A4;
constexpr static byte BUTTON5 = A5;

constexpr static byte RELAY1 = 4;
constexpr static byte RELAY2 = 5;

uint16_t remainTime = 0;
uint8_t specialTime = 0; //for mode 5
Mode mode;
DisplayMode dispMode;
bool runCountdown = false;
bool runSpecCountdown = false;
uint8_t displayArray[4];

TM1637Display display(DISPLAY_CLK, DISPLAY_DATA);

SimpleRelay relay1 = SimpleRelay(RELAY1, true);
SimpleRelay relay2 = SimpleRelay(RELAY2, true);

void setVariables(Mode m, const uint16_t t);
void setCounter(Mode m);
void setTime(Mode m);
void resetCounter(Mode m);
void countOverflow(Mode m);
void mainCounter();
void specialCounter();
void printTimeToSerial();

class FiveButtons : private virtual IOnClickListener, private virtual IOnPressListener {
public:
    FiveButtons() = default;

    void init();

    void update();

private:
    void onClick(ObjectButton &button) override;

    void onPress(ObjectButton &button) override;

    void onRelease(ObjectButton &button) override;

    void onLongPressStart(ObjectButton &button) override;

    void onLongPressEnd(ObjectButton &button) override;

    ObjectButton button1 = ObjectButton(BUTTON1);
    ObjectButton button2 = ObjectButton(BUTTON2);
    ObjectButton button3 = ObjectButton(BUTTON3);
    ObjectButton button4 = ObjectButton(BUTTON4);
    ObjectButton button5 = ObjectButton(BUTTON5);
};

void FiveButtons::onClick(ObjectButton &button) {
    switch (button.getId()) {
        case BUTTON1:
            Serial.println(F("B1 CLICKED"));
            setCounter(MODE_1);
            break;
        case BUTTON2:
            Serial.println(F("B2 CLICKED"));
            setCounter(MODE_2);
            break;
        case BUTTON3:
            Serial.println(F("B3 CLICKED"));
            setCounter(MODE_3);
            break;
        case BUTTON4:
            Serial.println(F("B4 CLICKED"));
            setCounter(MODE_4);
            break;
        case BUTTON5:
            Serial.println(F("B5 CLICKED"));
            setCounter(MODE_5);
        break;
    }
}

void FiveButtons::onPress(ObjectButton &button){

}

void FiveButtons::onRelease(ObjectButton &button){

}

void FiveButtons::onLongPressStart(ObjectButton &button) {
    switch (button.getId()) {
        case BUTTON1:
            Serial.println(F("B1 PRESSED"));
            resetCounter(MODE_1);
            break;
        case BUTTON2:
            Serial.println(F("B2 PRESSED"));
            resetCounter(MODE_2);
            break;
        case BUTTON3:
            Serial.println(F("B3 PRESSED"));
            resetCounter(MODE_3);
            break;
        case BUTTON4:
            Serial.println(F("B4 PRESSED"));
            resetCounter(MODE_4);
            break;
        case BUTTON5:
            Serial.println(F("B5 PRESSED"));
            resetCounter(MODE_5);
            break;
    }
}

void FiveButtons::onLongPressEnd(ObjectButton &button){

}

void FiveButtons::init() {
    button1.setDebounceTicks(DEBOUNCE);
    button1.setOnClickListener(this);
    button1.setClickTicks(CLICK_TICKS);
    button1.setOnPressListener(this);
    button1.setLongPressTicks(PRESS_TICKS);

    button2.setDebounceTicks(DEBOUNCE);
    button2.setOnClickListener(this);
    button2.setClickTicks(CLICK_TICKS);
    button2.setOnPressListener(this);
    button2.setLongPressTicks(PRESS_TICKS);

    button3.setDebounceTicks(DEBOUNCE);
    button3.setOnClickListener(this);
    button3.setClickTicks(CLICK_TICKS);
    button3.setOnPressListener(this);
    button3.setLongPressTicks(PRESS_TICKS);

    button4.setDebounceTicks(DEBOUNCE);
    button4.setOnClickListener(this);
    button4.setClickTicks(CLICK_TICKS);
    button4.setOnPressListener(this);
    button4.setLongPressTicks(PRESS_TICKS);

    button5.setDebounceTicks(DEBOUNCE);
    button5.setOnClickListener(this);
    button5.setClickTicks(CLICK_TICKS);
    button5.setOnPressListener(this);
    button5.setLongPressTicks(PRESS_TICKS);
}

void FiveButtons::update() {
    button1.tick();
    button2.tick();
    button3.tick();
    button4.tick();
    button5.tick();
}

FiveButtons fiveButtons = FiveButtons();

void setVariables(Mode m){
  if(m != mode && runCountdown == false){
      relay1.off();
      mode = m;
      setTime(mode);
      Serial.print(F("SET TO MODE "));
      Serial.println(mode);
    }
    else if(m == mode && runCountdown == false){
      relay1.on();
      runCountdown = true;
      Serial.println(F("RELAY 1 ON"));
    }
    /*else{ //pauza
      relay1.on();
      runCountdown = !runCountdown;
      Serial.print(F("COUNTDOWN STATE "));
      Serial.println(runCountdown);
    }*/
}

void setCounter(Mode m){
  switch(m){
    case MODE_1:
    setVariables(m);
    break;
    case MODE_2:
    setVariables(m);
    break;
    case MODE_3:
    setVariables(m);
    break;
    case MODE_4:
    setVariables(m);
    break;
    case MODE_5:
    setVariables(m);
    break;
    default:
    break;
  }
}

void printTimeToSerial(uint16_t t){
  Serial.print(F("TIME SET TO "));
  Serial.println(t);
}

void setTime(Mode m){
  switch(m){
      case MODE_1:
      remainTime = TIME1;
      printTimeToSerial(TIME1);
      break;
      case MODE_2:
      remainTime = TIME2;
      printTimeToSerial(TIME2);
      break;
      case MODE_3:
      remainTime = TIME3;
      printTimeToSerial(TIME3);
      break;
      case MODE_4:
      remainTime = TIME4;
      printTimeToSerial(TIME4);
      break;
      case MODE_5:
      remainTime = TIME5;
      printTimeToSerial(TIME5);
      specialTime = TIME5_SEC;
      break;
      default:
      break;
  }
}

void resetCounter(Mode m){
  if(m == mode){
    runCountdown = false;
    relay1.off();
    relay2.off();
    Serial.println(F("RESET"));
    setTime(m);
  }
}

void countOverflow(Mode m){
  relay1.off();
  setTime(m);
}

void setupTimer1() {
  noInterrupts();
  // Clear registers
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = 0;

  // 1 Hz (16000000/((15624+1)*1024))
  OCR1A = 15624;
  // CTC
  TCCR1B |= (1 << WGM12);
  // Prescaler 1024
  TCCR1B |= (1 << CS12) | (1 << CS10);
  // Output Compare Match A Interrupt Enable
  TIMSK1 |= (1 << OCIE1A);
  interrupts();
}

void mainCounter(){
  if(remainTime > 0){
    remainTime = remainTime - 1;
  }
  else if(remainTime < 1 && runSpecCountdown != true){
    runCountdown = false;
    countOverflow(mode);
  }
}

void specialCounter(){
  if((remainTime % TIME_SPECIAL) == 0){
    if(remainTime == 0){
      relay1.off();
    }
    relay2.on();
    Serial.println(F("RELAY 2 ON"));
    runSpecCountdown = true;
  }

  if(runSpecCountdown == true && specialTime > 0){
    specialTime = specialTime - 1;
  }
  else if(specialTime == 0){
    relay2.off();
    Serial.println(F("RELAY 2 OFF"));
    specialTime = TIME5_SEC;
    runSpecCountdown = false;
  }
}

ISR(TIMER1_COMPA_vect) {
  if(runCountdown == true){
    mainCounter();
    if(mode == MODE_5 && runCountdown == true){
      specialCounter();
    }
  }
}

void displayThings(){
  if(dispMode == SEC){
    display.showNumberDec(remainTime, false);
  }
  else{
    uint8_t min = remainTime / 60;
    if(min / 10 > 0){
      displayArray[0] = min / 10;
      displayArray[1] = min - 10 * displayArray[0];
    }
    else{
      displayArray[0] = 0;
      displayArray[1] = min;
    }

    uint8_t sec = remainTime % 60;
    if(sec / 10 > 0){
      displayArray[2] = sec / 10;
      displayArray[3] = sec - 10 * displayArray[2];
    }
    else{
      displayArray[2] = 0;
      displayArray[3] = sec;
    }

  displayArray[0] = display.encodeDigit(displayArray[0]);
	displayArray[1] = display.encodeDigit(displayArray[1]);
	displayArray[2] = display.encodeDigit(displayArray[2]);
	displayArray[3] = display.encodeDigit(displayArray[3]);
  displayArray[1] += 128;
  display.setSegments(displayArray);
  }
}

void setup() {

  // initialize serial communication
  #ifdef SERIAL_DEBUG_OUT
    Serial.begin(SERIAL_BAUDRATE);
    delay(1);
    Serial.println(F("Project name"));
    Serial.println(F("Version 1.0"));
    Serial.println(F("Developed by Arduino development"));
    Serial.println(F("development.arduino-shop.cz"));
    Serial.println();
  #endif

  mode = MODE_1;
  remainTime = TIME1;
  dispMode = MIN_SEC;
  relay1.off();
  relay2.off();
  setupTimer1();
  display.setBrightness(30);
  fiveButtons.init();
}

void loop() {
  displayThings();
  fiveButtons.update();
}

// #ifdef SERIAL_DEBUG_OUT
//  
// #endif

/**
	@brief
	@param
	@retval
*/
